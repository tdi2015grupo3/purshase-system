import redis
from oc_system import accept_order, reject_order, create_order, get_order
from cellar_system import get_almacenes, get_skus_with_stock, get_cuenta_fabrica, producir_stock, move_stock, move_stock_bodega, despachar_stock, get_stock
from b2b import notificate_order_accepted, new_order, notificate_order_rejected
from bill_system import bill as facturar
from bill_system import pay_invoice, get_invoice, reject_invoice
from mongo import save_order, delete_order, save_purchase, save_activity, update_order, save_offer, update_invoice
from bank_system import transfer
from production import PRODUCTS
from datetime import datetime, timedelta
import time
import json
from settings import CLIENTS
from tasks import despachar
import arrow
import twitter
from instagram.client import InstagramAPI
from settings import INSTAGRAM_CLIENT_ID, INSTAGRAM_CLIENT_SECRET, INSTAGRAM_CALLBACK
from settings import TWITTER_API_KEY, TWITTER_API_KEY_SECRET, TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET, TWITTER_ID, TWITTER_URL
from mule import save_twitt, get_price_for_sku
import odoo

ALMACEN_DESPACHO_ID = ''
ALMACEN_RECEPCION_ID = ''
ALMACEN_NORMAL1_ID = ''
ALMACEN_NORMAL2_ID = ''
BODEGA_PULMON_ID = ''
FABRICA_BANK_ACCOUNT = ''


def comprar(proveedor, sku, quantity, price, notes, delivery_date, order_id):
	"""
	Se compra una cantidad a un grupo de un producto determinado.
	"""
	print('Creating the order to buy...')
	order = {
		'canal': 'b2b',
		'quantity': quantity,
		'sku': sku,
		'proveedor': proveedor,
		'price': price,
		'notes': notes,
		'delivery_date': delivery_date
	}
	# Crea la orden en el sistema de ordenes de compra
	o = create_order(order)
	if o:
		print(o)
		activity = 'Se creo orden de compra del producto sku %s por %s unidades' % (sku, quantity)
		save_activity(order_id, activity)
		# Notifica al grupo
		n = new_order(o['_id'], proveedor)
		print('Se creo la orden de compra y se notifico al grupo proveedor.')
		# Se guarda en mongo la compra
		save_purchase(o)


def producir(sku, amount, production, order_id):
	"""
	Produces the specified stock.
	"""
	# Transfiero el monto a la fabrica
	print('Transfering ammount to the factory...')
	trx = transfer(amount, FABRICA_BANK_ACCOUNT)
	update_order(order_id, {'transaccion_fabrica': trx})
	activity = 'Se transfirio $%s a la fabrica.' % (amount)
	save_activity(order_id, activity) 
	# {u'monto': 125, u'created_at': u'2015-06-02T02:59:26.972Z', u'updated_at': u'2015-06-02T02:59:26.972Z', u'destino': u'55648ad2f89fed0300524ff3', u'origen': u'55648ad3f89fed0300525002', u'__v': 0, u'_id': u'556d1c0e6fed0e03009b7c3c'}
	trxId = trx['id']
	# Mando a producir
	p = producir_stock(sku, trxId, production)
	print('Se mando a producir ' + str(production) + ' del sku ' + sku)
	return p


def mover_a_despacho(sku, quantity):
	"""
	Mueve la cantidad quantity de productos sku al almacen de despacho.
	"""
	stock = get_stock(ALMACEN_NORMAL1_ID, sku)
	if len(stock) <= quantity:
		stock += get_stock(ALMACEN_NORMAL2_ID, sku)

	for a in range(quantity):
		move_stock(stock[a]['_id'], ALMACEN_DESPACHO_ID)
	print('Se movieron los productos a la bodega de despacho.')


# Procceses an order
def proccess_order(order):
	"""
	Se procesan las ordenes que llegan por cualquier canal de venta.
	order = {
			'order_id': order_id,
			'client': client,
			'proveedor': proveedor,
			'sku': sku,
			'date': date,
			'quantity': quantity,
			'price': price
		}
	"""

	print('Proccessing order: ' + order)
	today = datetime.today()
	today_string = today.strftime("%Y-%m-%d")
	# Valido que la orden de compra exista y que este bien
	json_order = json.loads(order)
	order_id = json_order['order_id']
	activity = 'Se recibio la orden de compra id %s' % (order_id)
	save_activity(order_id, activity)
	# Obtengo la orden del sistema de ordenes de comrpa.
	orden_list = get_order(order_id)

	# {u'sku': u'19', u'canal': u'b2b', u'proveedor': u'20', u'fechaEntrega': u'2015-10-10T03:00:00.000Z', u'notas': u'prueba', u'created_at': u'2015-06-02T01:57:10.616Z', u'updated_at': u'2015-06-02T01:57:10.616Z', u'cantidadDespachada': 0, u'cantidad': 150, u'__v': 0, u'precioUnitario': 1234, u'fechaDespachos': [], u'_id': u'556d0d76a928910300330968', u'estado': u'creada', u'cliente': u'3'}
	if orden_list:
		orden = orden_list[0]
		if orden.get('fechaEntrega', None) is None:
			print('No hay fecha de entrega por lo que no se puede procesar la orden.')
			return

		delivery_date = arrow.get(orden['fechaEntrega'])
		#Validar fecha entrega posterior a fecha actual
		if delivery_date < arrow.now():
			o = reject_order(order_id, 'FacturaVencida')
			save_activity(order_id, 'Se rechazo la orden debido a que la fecha de entrega es anterior a la fecha actual.')
			n = notificate_order_rejected(order_id, client)
			save_order(o[0])
			print('Se rechazo la orden debido a que la fecha de entrega es anterior a la fecha actual.')
			return

		sku = orden['sku']
		quantity = orden['cantidad']
		client = orden['cliente']
		canal = orden['canal']
		price = orden['precioUnitario']

		#Validar precio
		# mule_price = get_price_for_sku(sku)[0]['Precio']
		# if mule_price != price:
		# 	o = reject_order(order_id, 'PrecioInvalido')
		# 	save_activity(order_id, 'Se rechazo la orden debido a que el precio no corresponde al de la base de datos de Mule.')
		# 	n = notificate_order_rejected(order_id, client)
		# 	save_order(o[0])
		# 	print('Se rechazo la orden debido a que el precio no corresponde al de la base de datos de Mule.')
		# 	return

		# Reviso si tengo productos en bodega.
		# Reviso en el primer almacen 
		print('Getting skus...')
		skus1 = {s['_id']:s['total'] for s in get_skus_with_stock(ALMACEN_NORMAL1_ID)}
		skus2 = {s['_id']:s['total'] for s in get_skus_with_stock(ALMACEN_NORMAL2_ID)}
		# Si hay en el almacen 1
		stock = 0
		if sku in skus1:
			stock += skus1[sku]
		if sku in skus2:
			stock += skus2[sku]
		print('Stock: ' + str(stock))
		# HAY STOCK
		if stock >= quantity:
			# PROCESO la orden
			print('Stock disponible. Procesando la orden...')
			# Acepto la orden de compra
			print('Accepting the order...')
			o = accept_order(order_id)
			if o:
				save_activity(order_id, 'Se acepto la orden de compra.')
				# Guardo la orden en mongo
				save_order(o[0])
				print('Se guardo la orden en Mongo.')
				if canal == 'b2b':
					# Notifico al grupo
					print('Notificating the group...')
					n = notificate_order_accepted(order_id, client)
				# Emito la factura
				print('Billing...')
				# Emito la factura en el sistema de facturas
				bill = facturar(order_id)
				if bill:
					# Guardo la factura en la orden de compra en mongo.
					update_order(order_id, {'factura': bill})
					activity = 'Se emitio la factura (N %s)' % (bill['_id'])
					save_activity(order_id, activity)
					# Agrego la factura en odoo
					print('Agregando la factura a odo...')
					odoo.add_client_invoice(client, sku, quantity, bill['created_at'])
				else:
					print('Error emitiendo la factura')
				# Muevo a bodega de despacho
				print('Moving products to dispatch batch...')
				mover_a_despacho(sku, quantity)
				save_activity(order_id, 'Se movieron los productos al almacen de despacho.')
				# Si la fecha de entrega es para hoy la despacho de inmediato.
				if today_string == o[0]['fechaEntrega'].split('T')[0]:
					# Despacho Stock (con tarea)
					print('El despacho es para hoy. Enviando a despachar...')
					despachar.delay(sku, quantity, client, canal, price, order_id)
				
							 
		# NO HAY STOCK
		else:
			print('No Stock. Checking if there are materials to produce...')
			# Calculo la cantidad a transferir a la fabrica
			product = PRODUCTS[sku]
			production_time = product['production_time']
			production_batch = product['production_batch']
			production_cost = product['production_cost'] # Costo de producir un producto
			
			production = quantity
			if production%production_batch != 0:
				production = int(production/production_batch)*production_batch + production_batch
			amount = production*production_cost

			# Reviso si cuento con las materias primas
			req = True
			if product.get('components', None) is not None:
				for component in product['components']:
					prod_sku = component['sku']
					requirement = component['requirement']
					proveedor = component['proveedor']
					prod_batch = component['production_batch']
					prod_cost = component['production_cost']
					prod_time = component['production_time']

					s1 = {s['_id']:s['total'] for s in get_skus_with_stock(ALMACEN_NORMAL1_ID)}
					s2 = {s['_id']:s['total'] for s in get_skus_with_stock(ALMACEN_NORMAL2_ID)}
					
					prod_stock = 0
					if prod_sku in s1:
						prod_stock += s1[prod_sku]

					if prod_sku in s2:
						prod_stock += s2[prod_sku]

					if prod_stock < requirement*production:      
						if proveedor == '3':
							# Producir
							toproduce = int(requirement*quantity)
							if toproduce%prod_batch != 0:
								toproduce = int(toproduce/prod_batch)*prod_batch + prod_batch
							topay = toproduce*prod_cost
							p = producir(prod_sku, topay, toproduce, order_id)
							print(p)
							if p.get('disponible'):
								activity = 'Se mando a producir %s del producto sku %s porque no habia suficiente stock (stock: %s). Se estima que estaran disponibles para el %s' % (toproduce, prod_sku, prod_stock, p['disponible'])
								save_activity(order_id, activity)
								if arrow.get(p['disponible']) > delivery_date:
									req = False
						else:
							# Comprar
							req = False
							date = today + timedelta(days=7)
							activity = 'Se mando a comprar %s del producto sku %s porque no habia suficiente stock (stock: %s)' % (int(requirement*production), prod_sku, prod_stock)
							save_activity(order_id, activity)
							prod_mule_price = get_price_for_sku(prod_sku)[0]['Precio']
							comprar(proveedor, prod_sku, requirement*quantity, prod_mule_price, 'comprando', date, order_id)
							#comprar(proveedor, sku, quantity, price, notes, delivery_date):
			
			# Si tengo las materias primas mando a producir
			if req:
				p = producir(sku, amount, quantity, order_id)
				print(p)
				if p:
					# Veo si alcanzo a producir lo que me pidio
					print('Cecking if the production will be ready before delivery date...')
					if p.get('disponible'):
						activity = 'Se mando a producir %s del producto sku %s porque no habia suficiente stock (stock: %s)' % (quantity, sku, stock)
						save_activity(order_id, activity)
						if arrow.get(p['disponible']) < delivery_date:
							# Acepto la orden de compra
							print('Accepting the order...')
							o = accept_order(order_id)
							if o:
								save_activity(order_id, 'Se acepto la orden de compra.') 
								print('Guardo la orden en mongo.')
								save_order(o[0])
								print('Order accepted.')
								if canal == 'b2b':
									# Notifico al grupo
									n = notificate_order_accepted(order_id, client)
								# Emito la factura
								print('Billing...')
								bill = facturar(order_id)
								if bill:
									update_order(order_id, {'factura': bill})
									activity = 'Se emitio la factura (N %s)' % (bill['_id'])
									save_activity(order_id, activity)
									# Agrego la factura en odoo
									print('Agregando la factura a odoo...')
									odoo.add_client_invoice(client, sku, quantity, bill['created_at'])
								else:
									print('Error emitiendo la factura')
								# Guardo en mongo
						else:
							o = reject_order(order_id, 'NoStock')
							save_activity(order_id, 'Se rechazo la orden de compra debido a que no habia suficiente stock y la fabrica demoraba mas en producir que el tiempo que se tenia para entregar el pedido.')
							n = notificate_order_rejected(order_id, client)
							save_order(o[0])
							print('Se rechazo la orden de compra debido a que no habia suficiente stock y la fabrica demoraba mas en producir que el tiempo que se tenia para entregar el pedido.')
					else:
						o = reject_order(order_id, 'ErrorProduciendo')
						save_activity(order_id, 'Se rechazo la orden debido a error en produccion.')
						n = notificate_order_rejected(order_id, client)
						save_order(o[0])
						print('Se rechazo la orden debido a error en produccion.')
				else:
					o = reject_order(order_id, 'ErrorProduciendo')
					save_activity(order_id, 'Se rechazo la orden debido a error en produccion.')
					n = notificate_order_rejected(order_id, client)
					save_order(o[0])
					print('Se rechazo la orden debido a error en produccion.') 
			else:
				# No hay materias primas asique rechazo
				o = reject_order(order_id, 'NoStock')
				save_activity(order_id, 'Se rechazo la orden debido a que no hay suficientes materias primas para producir el producto.')
				n = notificate_order_rejected(order_id, client)
				save_order(o[0])
				print('Se rechazo la orden debido a que no hay suficientes materias primas para producir el producto.') 
	else:
		print('The order does not exist.')


def proccess_invoice(invoice_id):
	"""
	Procesa una factura. Si el precio corresponde a la orden de compra entonces se paga. Si no, se rechaza.
	Solo se procesa si se encuentra en estado pendiente.
	"""
	invoice = get_invoice(invoice_id)
	if invoice:
		invoice = invoice[0]
		if invoice['estado'] == 'pendiente':
			orden = get_order(invoice['oc'])
			if orden:
				orden = orden[0]
				#if invoice['total'] == orden['cantidad']*orden['precioUnitario']:
					#TODO: Pagar la factura cuando corresponda
				pay_invoice(invoice_id)
				print('Factura ' + invoice_id + ' pagada.')
				save_activity(orden['id'],'Factura ' + invoice_id + ' pagada.')
				
				# Add invoice to odoo
				end_date = (datetime.today() + timedelta(days=30)).strftime("%Y-%m-%d")
				odoo.add_provider_invoice(invoice['cliente'], orden['sku'], orden['cantidad'], orden['precioUnitario'], invoice['created_at'], invoice_id, end_date)
				# else:
				# 	reject_invoice(invoice_id, 'NoCuadraElValor')
				# 	print('Factura ' + invoice_id + ' rechazada.')
				# 	save_activity(orden['id'],'Factura ' + invoice_id + ' rechazada.')
		else:
			print('La factura ' + invoice_id +' ya se encuentra ' + invoice['estado'])


def process_invoice_paid(invoice_id):
	"""
	Procesa una factura que fue pagada
	"""
	invoice = get_invoice(invoice_id)
	if invoice:
		invoice = invoice[0]
		if invoice['estado'] == 'pagada':
			order_id = invoice['oc']
			update_invoice(order_id, 'pagada')
			save_activity(order_id, 'Factura pagada.')
			print('La factura %s ha sido pagada') % (invoice_id)

def process_invoice_rejected(invoice_id):
	"""
	Procesa una factura que fue rechazada
	"""
	invoice = get_invoice(invoice_id)
	if invoice:
		invoice = invoice[0]
		if invoice['estado'] == 'rechazada':
			order_id = invoice['oc']
			update_invoice(order_id, 'rechazada')
			save_activity(order_id, 'Factura rechazada.')
			print('La factura %s ha sido rechazada') % (invoice_id)


def process_order_accepted(order_id):
	"""
	Procesa una orden que fue aceptada por el grupo
	"""
	orden = get_order(order_id)
	if orden:
		orden = orden[0]
		if orden['estado'] == 'anulada':
			delete_order(order_id)
			save_activity(order_id,'Factura anulada por el proveedor.')

def process_order_canceled(order_id):
	"""
	Procesa una orden que fue cancelada por el grupo
	"""
	orden = get_order(order_id)
	if orden:
		orden = orden[0]
		if orden['estado'] == 'anulada':
			delete_order(order_id)
			save_activity(order_id,'Factura anulada por el proveedor.')

def proccess_offer(string_offer, api):
	"""
	Procesa una oferta que llega por rabbitmq o por instagram
	"""
	#print(string_offer)
	offer = json.loads(string_offer)
	#{'canal': 'Instagram', 'offer': {'changed_aspect': 'media', 'object': 'tag', 'object_id': 'promociones', 'time': 1435716150, 'subscription_id': 18717864, 'data': {}}}

	if offer['canal'] == 'Instagram':
		"""
		offer['offer'] = [
			{
				"subscription_id": "2",
				"object": "tag",
				"object_id": "promociones",
				"changed_aspect": "media",
				"time": 1297286541
			},
		]

		"""
		try:
			# api = InstagramAPI(access_token=INSTAGRAM_ACCESS_TOKEN)
			# api = InstagramAPI(client_id=INSTAGRAM_CLIENT_ID, client_secret=INSTAGRAM_CLIENT_SECRET)
			recent_media, next_ = api.tag_recent_media(1, None, offer['offer']['object_id'])
			caption = recent_media[0].caption.text
			instagram_id = recent_media[0].id
			#print(caption)
			#TODO: Verificar que sea una promocion del curso.
			if 'sku' in caption and 'precio' in caption and 'codigo' in caption:
				try:
					data = caption.split('#oferta ')[1].split(' ')
					sku = data[0].split('=')[1]
					price = data[1].split('=')[1]
					code = data[2].split('=')[1]
					#save_offer(canal, sku, price, code, instagram_id, init_date=None, end_date=None)
					save_offer('Instagram', sku, price, code, instagram_id)
					print('Oferta guardada.')
				except:
					print('Error codificando la oferta.')
			#TODO: Verificar que sea nueva.
			
		except Exception, e:
			print('Error obteniendo las imagenes de instagram: ' + str(e))
		pass

	elif offer['canal'] == 'RabbitMQ':
		print('New offer comming from RabbitMQ...')
		print(offer)
		"""
		offer['offer'] = 
			{
				sku : "",
				precio : int, 
				inicio : fecha, 
				fin : fecha
			}
		"""
		o = offer['offer']
		print(o['sku'])
		#TODO: Publicar en twitter
		try:
			api = twitter.Api(consumer_key=TWITTER_API_KEY, consumer_secret=TWITTER_API_KEY_SECRET, access_token_key=TWITTER_ACCESS_TOKEN, access_token_secret=TWITTER_ACCESS_TOKEN_SECRET)
			text = 'Nueva oferta del producto sku=%s! Precio: $%s, Inicio: %s, Fin: %s' % (o['sku'], o['precio'], o['inicio'], o['fin'])
			twitt = api.PostUpdate(text)
			# Save twitt url to Mule
			twitt_url = TWITTER_URL + str(twitt.id)
			save_twitt(twitt_url)
		except Exception, e:
			print('Error accediendo a twitter: ' + str(e))

		#save_offer(canal, sku, price, code, instagram_id, init_date=None, end_date=None):
		save_offer('RabbitMQ', o['sku'], o['precio'], None, None, o['inicio'], o['fin'])
		print('Oferta guardada.')



def start():
	"""
	Starts the pubsub queue in redis.
	"""

	print('Running Purchase System ...')

	# Global variables
	global ALMACEN_DESPACHO_ID
	global ALMACEN_RECEPCION_ID
	global BODEGA_PULMON_ID
	global ALMACEN_NORMAL1_ID
	global ALMACEN_NORMAL2_ID
	global FABRICA_BANK_ACCOUNT
	
	# Get Almacenes from cellar system
	print('Getting Almacenes ids...')
	almacenes = get_almacenes()
	for almacen in almacenes:
		if almacen['despacho']:
			ALMACEN_DESPACHO_ID = almacen['_id']
		elif almacen['recepcion']:
			ALMACEN_RECEPCION_ID = almacen['_id']
		elif almacen['pulmon']:
			BODEGA_PULMON_ID = almacen['_id']
		else:
			if ALMACEN_NORMAL1_ID == '':
				ALMACEN_NORMAL1_ID = almacen['_id']
			else:
				ALMACEN_NORMAL2_ID = almacen['_id']
	
	print('Getting fabrica bank account...')
	FABRICA_BANK_ACCOUNT = get_cuenta_fabrica()['cuentaId']

	# subscribe to instagram offers
	print('Subscribing to instagram...')
	api = InstagramAPI(client_id=INSTAGRAM_CLIENT_ID, client_secret=INSTAGRAM_CLIENT_SECRET)
	#Subscribe to all media tagged with 'promocion' and 'promociones'
	api.create_subscription(object='tag', object_id='promocion', aspect='media', callback_url=INSTAGRAM_CALLBACK)
	api.create_subscription(object='tag', object_id='promociones', aspect='media', callback_url=INSTAGRAM_CALLBACK)

	try:
		# Cola REDIS PUB/SUB
		print('Creating the REDIS Pub/Sub and subscribing to events...')
		r = redis.StrictRedis()
		p = r.pubsub(ignore_subscribe_messages=True)
		p.subscribe('new-purchase', 'new-offer', 'production-ready', 'order-canceled', 'order-rejected', 'order-accepted', 'invoice-paid', 'invoice-rejected', 'invoice-created')

		for message in p.listen():
			print("Channel: %s, Message: %s") % (message['channel'], message['data'])
			
			# Nueva orden de compra
			if message['channel'] == 'new-purchase':
				print('Nueva orden de compra recibida.')
				order = message['data']
				proccess_order(order)

			elif message['channel'] == 'invoice-created':
				print('Se ha emitido una factura a la empresa.')
				invoice_id = message['data']
				proccess_invoice(invoice_id)

			elif message['channel'] == 'order-accepted':
				print('La orden de compra fue aceptada')
				order_id = message['data']
				process_order_accepted(order_id)

			elif message['channel'] == 'order-canceled':
				print('La orden de compra fue cancelada')
				order_id = message['data']
				process_order_canceled(order_id)

			elif message['channel'] == 'invoice-paid':
				print('La factura fue pagada')
				invoice_id = message['data']
				process_invoice_paid(invoice_id)

			elif message['channel'] == 'invoice-rejected':
				print('La factura fue rechazada')
				invoice_id = message['data']
				process_invoice_rejected(invoice_id)

			# Nueva oferta
			elif message['channel'] == 'new-offer':
				offer = message['data']
				proccess_offer(offer, api)

			# Produccion lista
			elif message['channel'] == 'production-ready':
				#Sigo con el pedido
				pass

	except Exception as e:
		print('Error: ' + str(e))


if __name__ == '__main__':
	start()