import requests
import json
from settings import CLIENTS

def new_order(order_id, group):
	"""
	Notificates the group that an order was created in the order system.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/new_order.json'
			params = {
				"order_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/create_order/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'order_id': order_id}
			r = requests.post(url, headers=headers, data=json.dumps(payload))
		else:	
			url = CLIENTS[group]['url'] + '/new_order/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'order_id': order_id}
			r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except Exception, e:
		print('Error in b2b: ' + str(e))
		response = None
	return response

def notificate_order_accepted(order_id, group):
	"""
	Notificates the group that an order was accepted.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/order_accepted.json'
			params = {
				"order_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/accepted_order/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'order_id': order_id}
			r = requests.put(url, headers=headers, data=json.dumps(payload))
		else:
			url = CLIENTS[group]['url'] + '/order_accepted/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'order_id': order_id}
			if group == '1':
				r = requests.put(url, headers=headers, data=json.dumps(payload))
			else:
				r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except:
		response = None
	return response

def notificate_order_rejected(order_id, group):
	"""
	Notificates the group that an order was rejected.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/order_rejected.json'
			params = {
				"order_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/rejected_order/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'order_id': order_id}
			r = requests.put(url, headers=headers, data=json.dumps(payload))
		else:
			url = CLIENTS[group]['url'] + '/order_rejected/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'order_id': order_id}
			if group == '1':
				r = requests.delete(url, headers=headers, data=json.dumps(payload))
			else:
				r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except:
		response = None
	return response

def notificate_order_canceled(order_id, group):
	"""
	Notificates the group that an order was canceled.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/order_canceled.json'
			params = {
				"order_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/canceled_order/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'order_id': order_id}
			r = requests.delete(url, headers=headers, data=json.dumps(payload))
		else:
			url = CLIENTS[group]['url'] + '/order_canceled/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'order_id': order_id}
			if group == '1':
				r = requests.delete(url, headers=headers, data=json.dumps(payload))
			else:
				r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except Exception, e:
		response = None
		print('Error: ' + str(e))
	return response

def notificate_invoice_created(invoice_id, group):
	"""
	Notificates the group that an invoice was created.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/invoice_created.json'
			params = {
				"invoice_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/issued_invoice/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'invoice_id': order_id}
			r = requests.post(url, headers=headers, data=json.dumps(payload))

		else:
			url = CLIENTS[group]['url'] + '/invoice_created/'
			if group == '1':
				url = CLIENTS[group]['url'] + '/new_invoice/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'invoice_id': invoice_id}
			r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except Exception, e:
		response = None
		print('Error: ' + str(e))
	return response

def notificate_invoice_paid(invoice_id, group):
	"""
	Notificates the group that an invoice was paid.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/invoice_paid.json'
			params = {
				"invoice_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/invoice_paid/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'invoice_id': order_id}
			r = requests.put(url, headers=headers, data=json.dumps(payload))
		else:
			url = CLIENTS[group]['url'] + '/invoice_paid/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'invoice_id': invoice_id}
			r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except Exception, e:
		response = None
		print('Error: ' + str(e))
	return response

def notificate_invoice_rejected(invoice_id, group):
	"""
	Notificates the group that an invoice was rejected.
	"""
	try:
		if group == '4':
			url = CLIENTS[group]['url'] + '/invoice_rejected.json'
			params = {
				"invoice_id": order_id,
				"token": CLIENTS[group]['token']
			}
			r = requests.get(url, params=params)
		elif group == '7':
			url = CLIENTS[group]['url'] + '/rejected_invoice/'
			headers = {'authorization': CLIENTS[group]['token']}
			payload = {'invoice_id': order_id}
			r = requests.put(url, headers=headers, data=json.dumps(payload))
		else:
			url = CLIENTS[group]['url'] + '/invoice_rejected/'
			authorization = 'Token %s' % (CLIENTS[group]['token'])
			headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': authorization }
			payload = {'invoice_id': invoice_id}
			if group == '1':
				r = requests.delete(url, headers=headers, data=json.dumps(payload))		
			else:
				r = requests.post(url, headers=headers, data=json.dumps(payload))
		response = r.json()
	except Exception, e:
		response = None
		print('Error: ' + str(e))
	return response