
#celery -A tasks worker -B --loglevel=info
#celery -A tasks beat --loglevel=info
import pysftp
import redis
from celery import Celery
from celery.schedules import crontab
from celery.task import task, periodic_task
from lxml import etree
import json
from settings import FTP_HOST as HOST_NAME
from settings import BROKER_URL, CLIENTS, RECEPTION_IDS, FTP_PASSWORD
from mongo import get_orders_for_today, delete_order, update_state, save_activity
from cellar_system import get_stock, get_almacenes, despachar_stock, move_stock_bodega, get_skus_with_stock, get_stock, move_stock

app = Celery('tasks', broker=BROKER_URL)

# Redis client to publish new purchases
r = redis.StrictRedis()


@app.task
def despachar(sku, quantity, client, canal, price, order_id):
	"""
	Despacha la cantidad quantity de productos sku desde la bodega de despacho hasta el almacen destiny.
	"""
	ALMACEN_DESPACHO_ID = ''
	almacenes = get_almacenes()
	for almacen in almacenes:
		if almacen['despacho']:
			ALMACEN_DESPACHO_ID = almacen['_id']
			break
	stock = get_stock(ALMACEN_DESPACHO_ID, sku)
	for a in range(quantity):
		if canal == 'ftp':
			despachar_stock(stock[a]['_id'], CLIENTS[client]['address'], price, order_id)
		else:
			destiny = RECEPTION_IDS[client]
			move_stock_bodega(stock[a]['_id'], destiny)
	save_activity(order_id, 'Los productos fueron despachados')
	update_state(order_id, 'finalizada')
	print('Se despacharon los productos al almacen exitosamente')



################# PERIODIC TASKS ##################


@periodic_task(run_every=crontab(minute=0, hour=9))
def check_for_dispatch():
	"""
	Checks for orders to dispatch. Todos los dias a las 9 horas.
	"""
	orders = get_orders_for_today()
	for order in orders:
		order_id = order['_id']
		sku = order['sku']
		canal = order['canal']
		quantity = order['cantidad']
		client = order['cliente']
		price = order['precioUnitario']
		# Despacho los productos
		despachar.delay(sku, quantity, client, canal, price, order_id)



@periodic_task(run_every=crontab(minute='*/30'))
def check_for_move():
	"""
	Checks for orders to dispatch. Todos los dias a las 9 horas.
	"""
	ALMACEN_RECEPCION_ID = ''
	ALMACEN_NORMAL1_ID = ''
	almacen1_abailable = 0
	ALMACEN_NORMAL2_ID = ''
	almacen2_abailable = 0
	almacenes = get_almacenes()
	for almacen in almacenes:
		if almacen['despacho']:
			pass
		elif almacen['recepcion']:
			ALMACEN_RECEPCION_ID = almacen['_id']
		elif almacen['pulmon']:
			pass
		else:
			if ALMACEN_NORMAL1_ID == '':
				ALMACEN_NORMAL1_ID = almacen['_id']
				almacen1_available = almacen['totalSpace'] - almacen['usedSpace']
			else:
				ALMACEN_NORMAL2_ID = almacen['_id']
				almacen2_available = almacen['totalSpace'] - almacen['usedSpace']

	skus_dic = {s['_id']:s['total'] for s in get_skus_with_stock(ALMACEN_RECEPCION_ID)}
	skus = skus_dic.keys()
	for sku in skus:
		stock = get_stock(ALMACEN_RECEPCION_ID, sku)
		for prod in stock:
			if almacen1_available > 0:
				move_stock(prod['_id'], ALMACEN_NORMAL1_ID)
				almacen1_available -=1
			elif almacen2_available > 0:
				move_stock(prod['_id'], ALMACEN_NORMAL2_ID)
				almacen2_available -=1
			else:
				print('No se pueden mover mas productos porque los almacenes estan llenos')
				return

		

@periodic_task(run_every=crontab(minute='*/10'))
def check_for_oc():
	"""
	Checks for new orders each 10 minutes
	"""
	with pysftp.Connection(HOST_NAME, username='integra3', password=FTP_PASSWORD) as sftp:
		for file_name in sftp.listdir():
			if file_name.endswith(".xml"):
				# Gets the order to local
				local_dir = 'pedidos/%s' % (file_name)
				sftp.get(file_name, local_dir, preserve_mtime=True)
				order = read_xml(local_dir)
				if order['success']:
					r.publish('new-purchase', json.dumps(order['order']))
					# Deletes the order from remote
					sftp.remove(file_name)


def read_xml(file_name):
	"""
	Reads an xml file and creates an order
	"""

	try:
		xml_file = open(file_name, 'r')
		xml = xml_file.read()
		xml_file.close()
		order = etree.fromstring(xml).find('Pedido')
	except:
		print('No file found with that name.')
		return {'success': False, 'reason': 'No file found with that name.'}
	
	try:
		order_id = order.find('oc').text
		client = order.find('cliente').text
		proveedor = order.find('proveedor').text
		sku = order.find('sku').text
		date = order.find('fechaEntrega').text
		quantity = order.find('cantidad').text
		price = order.find('precioUnitario').text

		order_object = {
			'order_id': order_id,
			'client': client,
			'proveedor': proveedor,
			'sku': sku,
			'date_delivery': date,
			'quantity': quantity,
			'price': price
		}
		return {'success': True, 'order': order_object}
	except:
		print('Error parsing the XML file.')
		return {'success': False, 'reason': 'Error parsing the XML file.'}
