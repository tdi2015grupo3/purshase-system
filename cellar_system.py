# Connects to de cellar system
import requests
from authorization import get_hash
import json
from settings import CELLAR_URL as URL

"""
request:
http://docs.python-requests.org/en/latest/user/quickstart/#
"""

HEADER = 'INTEGRACION grupo3:'

def get_almacenes():
	"""
	- Descripcion: Entrega informacion sobre los almacenes de la bodega solicitada
	- Method: GET
	- Url: /almacenes
	- Parametros:
	- Retorno:
		- Almacenes[]
	- Authorization: GET (no hay parametros) 	
	"""

	url = URL + '/almacenes'
	auth_header = HEADER + get_hash('GET','')
	headers = {'Authorization': auth_header}
	r = requests.get(url, headers=headers)
	response = r.json()
	return response

def get_skus_with_stock(almacen_id):
	"""
	- Descripcion: Entrega una lista de skus que tienen stock en un almacen
	- Method: GET
	- Url: /skusWithStock
	- Parametros:
		- almacenId
	- Retorno: Arreglo de objetos:
		{
			_id: sku,
			total: int
		}
	- Authorization: GETalmacenId (reemplazar con el valor del parametro)
	"""

	url = URL + '/skusWithStock'
	auth_header = HEADER + get_hash('GET', almacen_id)
	headers = {'Authorization': auth_header}
	payload = {'almacenId': almacen_id}
	r = requests.get(url, headers=headers, params=payload)
	response = r.json()
	return response


def get_stock(almacen_id, sku, limit=None):
	"""
	- Descripcion: Devuelve todos los productos que se encuentran guardados en un almacen
	- Method: GET
	- Url: /stock
	- Parametros:
		- almacenId
		- sku
		- limit (opcional): limite de productos a entregar. Por defecto 100, maximo 200
	- Retorno:
		- Productos[]
	- Authorization: GETalmacenIdsku (reemplazar con valores de parametros)
	"""
	url = URL + '/stock'
	if limit is not None:
		param = '%s%s%s' % (almacen_id, sku, limit)
	else:
		param = '%s%s' % (almacen_id, sku)
	auth_header = HEADER + get_hash('GET', param)
	headers = {'Authorization': auth_header}
	payload = {'almacenId': almacen_id, 'sku': sku, 'limit': limit}
	r = requests.get(url, headers=headers, params=payload)
	response = r.json()
	return response

def move_stock(product_id, almacen_id):
	"""
	- Descripcion: Mueve un producto de un almacen a otro, siempre y cuando haya espacio en el almacen de destino.
	- Method: POST
	- Url: /moveStock
	- Parametros:
		- productoId
		- almacenId: Almacen de detino
	- Retorno:
		- Producto
	- Errores:
		- Producto no encontrado
		- Almacen no encontrado
		- Traspaso no realizado debido a falta de espacio.
	- Authorization: POSTproductoIdalmacenId (reemplazar con valores de parametros)
	"""
	url = URL + '/moveStock'
	param = '%s%s' % (product_id, almacen_id)
	auth_header = HEADER + get_hash('POST', param)
	headers = {'Content-Type': 'application/json', 'Authorization': auth_header}
	payload = {'productoId': product_id, 'almacenId': almacen_id}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response


def move_stock_bodega(product_id, almacen_id):
	"""
	- Descripcion: Mueve un producto de una bodega a otra, siempre y cuando haya espacio en el almacen de recepcion de la bodega de destino.
	- Method: POST
	- Url: /moveStockBodega
	- Parametros:
		- productoId
		- almacenId: Almacen de recepcion de la bodega del grupo de destino
	- Retorno:
		- Producto
	- Errores:
		- Producto no encontrado
		- Bodega no encontrada
		- Traspaso no realizado debido a falta de espacio.
		- Producto no se encuentra en bodega de despacho.
	- Authorization: POSTproductoIdalmacenId (reemplazar con valores de parametros)
	"""
	url = URL + '/moveStockBodega'
	param = '%s%s' % (product_id, almacen_id)
	auth_header = HEADER + get_hash('POST', param)
	headers = {'Content-Type': 'application/json', 'Authorization': auth_header}
	payload = {'productoId': product_id, 'almacenId': almacen_id}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response


def despachar_stock(product_id, address, price, order_id):
	"""
	- Descripcion: Envia un stock a un cliente desde la bodega de despacho.
	- Method: DELETE
	- Url: /stock
	- Parametros:
		- productoId
		- direccion: string
		- precio: int, precio al que se vendio el producto.
		- ordenDeCompraId: string, id de la orden de compra para el cual se esta despachando.
	- Retorno:
		- Boolean, true si despacho se curso correctamente, false en otro caso.
	- Errores:
		- Producto no encontrado
	- Authorization: DELETEproductoIddireccionpreciopedidoId (reemplazar con valores de parametros)
	"""
	url = URL + '/stock'
	param = '%s%s%s%s' % (product_id, address, price, order_id)
	auth_header = HEADER + get_hash('DELETE', param)
	headers = {'Authorization': auth_header}
	payload = {'productoId': product_id, 'direccion': address, 'precio': price, 'pedidoId': order_id}
	r = requests.delete(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response


def producir_stock(sku, transaction_id, quantity):
	"""
	- Descripcion: Solicitud de producir stock de un nuevo producto.
	- Method: PUT
	- Url: /fabrica/fabricar
	- Parametros:
		- sku: string
		- trxId: string
		- cantidad: int
	- Retorno:
		- Boolean, true si pedido de produccion se curso correctamente, false en otro caso.
	- Errores:
		- Pago incorrecto
		- Materias primas insuficientes
	- Authorization: PUTskucantidadtransaccionId (reemplazar con valores de parametros)
	"""
	url = URL + '/fabrica/fabricar'
	param = '%s%s%s' % (sku, quantity, transaction_id)
	auth_header = HEADER + get_hash('PUT', param)
	headers = {'Content-Type': 'application/json', 'Authorization': auth_header}
	payload = {'sku': sku, 'trxId': transaction_id, 'cantidad': quantity}
	r = requests.put(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def get_cuenta_fabrica():
	"""
	- Descripcion: Entrega numero de cuenta de la fabrica.
	- Method: GET
	- Url: /fabrica/fabricar
	- Parametros:
	- Retorno:
		- Numero de cuenta.
	- Errores:
	- Authorization: GET
	"""
	url = URL + '/fabrica/getCuenta'
	auth_header = HEADER + get_hash('GET', '')
	headers = {'Authorization': auth_header}
	r = requests.get(url, headers=headers)
	response = r.json()
	return response

