# File that connects to the O.C. System
import requests
import json
from settings import OC_URL as URL
from datetime import datetime
import time

"""
request:
http://docs.python-requests.org/en/latest/user/quickstart/#
"""

def create_order(order):
	"""
	Creates a new order in the O.C. System
	oder['delivery_date'] es de la forma: datetime(2015,5,15)
	"""

	url = URL + '/crear'
	payload = {
		'canal': order['canal'], 
		'cantidad': order['quantity'], 
		'sku': order['sku'], 
		'proveedor': order['proveedor'], 
		'precioUnitario': order['price'], 
		'notas': order['notes'],
		'cliente': "3",
		'fechaEntrega': int(time.mktime(order['delivery_date'].timetuple())*1000)
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.put(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def reject_order(order_id, cause):
	"""
	Rejects an order.
	Descripcion: Permite a un proveedor rechazar una orden de compra creada por un cliente.
	url: /rechazar/:id
	parametros:
		- 'rechazo': motivo por el cual se rechaza
	retorno: La orden de compra o error.
	"""
	url = URL + '/rechazar/' + order_id
	headers = {'Content-Type': 'application/json'}
	payload = {'rechazo': cause}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def accept_order(order_id):
	"""
	Accepts the order
	"""
	url = URL + '/recepcionar/' + order_id
	r = requests.post(url)
	response = r.json()
	return response

def cancel_order(order_id, cause):
	"""
	Cancels an order (la anula)
	"""
	url = URL + '/anular/' + order_id
	headers = {'Content-Type': 'application/json'}
	payload = {'anulacion': cause}
	r = requests.delete(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def get_order(order_id):
	"""
	Gets an order
	"""
	try:
		url = URL + '/obtener/' + order_id
		r = requests.get(url)
		response = r.json()
		return response
	except:
		return None

def dispatch_product(order_id, sku, quantity):
	"""
	Marcar productos despachados de una orden de compra en bodega.
	"""
	pass