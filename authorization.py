from hashlib import sha1
from settings import PRIVATE_KEY
import hmac

def get_hash(method, parameters):
    
    # The Base String as specified here: 
    raw = method + parameters # as specified by oauth
    hashed = hmac.new(PRIVATE_KEY, raw, sha1)
    # The signature
    return hashed.digest().encode("base64").rstrip('\n')