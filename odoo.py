from settings import ODOO_URL, ODOO_DB_NAME, ODOO_USERNAME, ODOO_PASSWORD, ODOO_PORT, CLIENTS
import odoorpc
from mule import get_price_for_sku
from production import PRODUCTS

def connect_to_odoo():
	odoo = odoorpc.ODOO(ODOO_URL, port=ODOO_PORT)
	odoo.login(ODOO_DB_NAME, ODOO_USERNAME, ODOO_PASSWORD)
	return odoo

def add_client_invoice(client_id, sku, quantity, date):
	"""
	Agrega una factura de cliente a ODOO
	"""
	# Get invoice data
	invoice_date = date.split('T')[0] #'2015-06-26'
	product = PRODUCTS[sku]
	client = CLIENTS[client_id]

	# Get the price of the sku
	p = get_price_for_sku(sku)
	price = p[0]['Precio']

	if price is not None:
		# Connect to odoo
		odoo = connect_to_odoo()

		# Get Invoice table
		Inv = odoo.env['account.invoice']
		
		# Create the invoice in odoo
		inv = { 'date_invoice': invoice_date, 'partner_id': client['odoo_id'], 'account_id': 16 } # Cuentas por cobrar
		i = Inv.create(inv)

		# Get Invoice.line table
		Inv_line = odoo.env['account.invoice.line']

		# Create the invoice line
		#TODO: Agregar los odoo_id
		inv_line = { 
			'product_id': product['odoo_id'], 'quantity': quantity, 'account_id': 16, 'invoice_id': i, 
			'price_unit': price, 'name': product['name']
		}
		il = Inv_line.create(inv_line)

		# Le agrego el impuesto
		iln = Inv_line.browse(il)
		iln.invoice_line_tax_id =  [1]


def add_provider_invoice(provider_id, sku, quantity, price, date, invoice_id, end_date):
	"""
	Agrega una factura de proveedor a ODOO
	"""
	#{u'proveedor': u'3', u'iva': 21745, u'bruto': 114443, u'created_at': u'2015-06-30T07:51:32.689Z', 
	#u'oc': u'55924836ae99100300fb4e8c', u'updated_at': u'2015-06-30T07:51:32.689Z', u'__v': 0, 
	#u'total': 136188, u'_id': u'55924a84ae99100300fb4e94', u'estado': u'pendiente', u'cliente': u'16'}


	# Get invoice data
	invoice_date = date.split('T')[0] #'2015-06-26'
	product = PRODUCTS[sku]
	client = CLIENTS[provider_id]
	
	# Connect to odoo
	odoo = connect_to_odoo()

	# Get Invoice table
	Inv = odoo.env['account.invoice']
	
	# Create the invoice in odoo
	inv = { 
		'date_invoice': invoice_date, 'partner_id': client['odoo_provider_id'], 'account_id': 58,
		'origin': invoice_id, 'date_due': end_date, 'type': 'in_invoice' 
	} # Cuentas por pagar
	i = Inv.create(inv)

	# Get Invoice.line table
	Inv_line = odoo.env['account.invoice.line']

	# Create the invoice line
	#TODO: Agregar los odoo_id
	inv_line = { 
		'product_id': product['odoo_id'], 'quantity': quantity, 'account_id': 58, 'invoice_id': i, 
		'price_unit': price, 'name': product['name']
	}
	il = Inv_line.create(inv_line)

	# Le agrego el impuesto
	iln = Inv_line.browse(il)
	iln.invoice_line_tax_id =  [2]







