import requests
import json
from settings import MULE_URL as URL
from datetime import datetime

def get_price_for_sku(sku):
	"""
	Retorna el precio del sku en la fecha dada.
	"""
	url = URL + '/precios'
	params = {
		"sku": sku,
		"fecha":  datetime.today().strftime("%Y-%m-%d") #'2015-mes-dia'
	}
	r = requests.get(url, params=params)
	response = r.json()
	return response

def save_twitt(url):
	"""
	Guarda un twitt en la base de datos.
	"""
	url = URL + '/tweets'
	params = { "url": url }
	r = requests.get(url, params=params)
	response = r.json()
	print(response)
	return response