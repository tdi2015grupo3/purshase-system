from pymongo import MongoClient
from datetime import datetime, timedelta

#Client
client = MongoClient()
db = client['integra3']

def save_offer(canal, sku, price, code, instagram_id, init_date=None, end_date=None):
	"""
	Guarda una oferta
	"""
	coll = db['offers']
	if init_date is None:
		init_date = datetime.now()
	if end_date is None:
		end_date =  init_date + timedelta(days=1)

	offer = {'canal': canal, 'sku': sku, 'price': price, 'code': code, 'init_date': init_date, 'end_date': end_date, 'instagram_id': instagram_id}
	coll.insert_one(offer)

def get_offer():
	"""
	Obtiene todas las ofertas guardadas en mongo.
	"""
	coll = db.offers
	return coll.find()

def get_orders():
	"""
	Obtiene todas las ordenes guardadas en mongo
	"""
	coll = db.orders
	docs = coll.find()
	return docs

def save_activity(order_id, activity):
	"""
	Guarda una actividad relacionada a la orden de compra
	"""
	coll = db['activities']
	now = datetime.now()
	doc = {'datetime': now, 'activity': activity, 'order_id': order_id}
	coll.insert_one(doc)

def save_order(order, bill=None):
	"""
	Guarda una orden en mongo para que sea despachada en su delivery date
	{u'sku': u'19', u'canal': u'b2b', u'proveedor': u'20', u'fechaEntrega': u'2015-10-10T03:00:00.000Z', u'notas': u'prueba', u'created_at': u'2015-06-02T01:57:10.616Z', u'updated_at': u'2015-06-02T01:57:10.616Z', u'cantidadDespachada': 0, u'cantidad': 150, u'__v': 0, u'precioUnitario': 1234, u'fechaDespachos': [], u'_id': u'556d0d76a928910300330968', u'estado': u'creada', u'cliente': u'3'}

	"""
	coll = db['orders']
	order['invoice'] = bill
	coll.insert_one(order)

def delete_order(order_id):
	"""
	Elimina una orden de mongo para que no sea despachada si es que se cancelo.
	"""
	coll = db['orders']
	doc = coll.find_one({'_id': order_id})
	if doc:
		coll.delete_one({'_id': doc['_id']})

def get_orders_for_today():
	"""
	Retorna todas las ordenes que hay que despachar hoy
	"""
	today = datetime.today().strftime("%Y-%m-%d")
	coll = db['orders']
	regex = '^%s' % (today)
	docs = coll.find({'fechaEntrega': { '$regex': regex }, 'cantidadDespachada': 0, 'estado': 'aceptada'})
	return docs


def save_purchase(purchase):
	"""
	Guarda en mongo una compra realizada por nosotros
	"""
	coll = db['purchases']
	coll.insert_one(purchase)

def update_state(order_id, state):
	"""
	Actualiza el estado de la orden en mongo
	"""
	coll = db['orders']
	doc = coll.update_one({'_id': order_id}, { '$set': {'estado': state}})


def update_order(order_id, update):
	"""
	Actualiza el estado de la orden en mongo
	"""
	coll = db['orders']
	doc = coll.update_one({'_id': order_id}, { '$set': update })

def update_invoice(order_id, invoice_state):
	"""
	Actualiza el estado de una factura de orden de compra
	"""
	coll = db['orders']
	order = coll.find_one({'_id': order_id})
	factura = order['factura']
	factura['estado'] = invoice_state
	order = coll.update_one({'_id': order_id}, { '$set': { 'factura': factura }})
	