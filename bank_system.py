#File that connects with the bank system
import requests
import json
from datetime import datetime
import time
from settings import BANK_URL as URL
from settings import ACCOUNT_ID

def transfer(quantity, destiny_account):
	"""
	Transfers money to the destiny account
	"""
	url = URL + '/trx'
	payload = {
		'monto': quantity, 
		'cuentaOrigen': ACCOUNT_ID, 
		'cuentaDestino': destiny_account
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.put(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def get_transaction(transaction_id):
	"""
	Gets a transaction
	"""
	url = URL + '/trx/' + transaction_id 
	r = requests.get(url)
	response = r.json()
	return response

def get_cartola(init_date, end_date, limit=None):
	"""
	Gets the cartola from a given time
	"""
	url = URL + '/cartola'
	payload = {
		'id': ACCOUNT_ID, 
		'inicio': int(time.mktime(init_date.timetuple())*1000), 
		'fin': int(time.mktime(end_date.timetuple())*1000),
		'limit': limit
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def get_account():
	"""
	Gets the account
	"""
	url = URL + '/cuenta/' + ACCOUNT_ID
	r = requests.get(url)
	response = r.json()
	return response