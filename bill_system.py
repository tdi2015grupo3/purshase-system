# File that connects with the Billing System
import requests
import json
from settings import BILLING_URL as URL
from settings import GROUP_ID

def bill(order_id):
	"""
	Emite una factura a partir de una orden de compra
	"""
	url = URL + '/'
	payload = {
		'oc': order_id, 
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.put(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def get_invoice(invoice_id):
	"""
	Obtiene una factura
	"""
	url = URL + '/' + invoice_id
	r = requests.get(url)
	response = r.json()
	return response

def pay_invoice(invoice_id):
	"""
	Permite pagar una factura
	"""
	url = URL + '/pay'
	payload = {
		'id': invoice_id, 
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def reject_invoice(invoice_id, cause):
	"""
	Permite rechazar una factura
	"""
	url = URL + '/reject'
	payload = {
		'id': invoice_id,
		'motivo': cause 
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def cancel_invoice(invoice_id, cause):
	"""
	Permite anular una factura
	"""
	url = URL + '/cancel'
	payload = {
		'id': invoice_id,
		'motivo': cause 
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.post(url, headers=headers, data=json.dumps(payload))
	response = r.json()
	return response

def create_boleta(client, total):
	"""
	Permite crear una boleta para un cliente final.
	"""
	url = URL + '/boleta'
	payload = {
		'proveedor': GROUP_ID,
		'cliente': client,
		'total': total 
	}
	headers = {'Content-Type': 'application/json'}
	r = requests.put(url, headers=headers, data=json.dumps(payload))
	# response = r.json()
	# return response
	return r

