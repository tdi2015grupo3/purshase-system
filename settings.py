# -*- coding: utf-8 -*-
# SETTINGS
PRODUCTION = True

# Celery
BROKER_URL = 'redis://localhost:6379/0'

if PRODUCTION:
	# MULE
	MULE_URL = 'http://moyas.ing.puc.cl/integra3'
	# ODOO
	ODOO_URL = 'moyas.ing.puc.cl'
	# Ftp host
	FTP_HOST = 'moyas.ing.puc.cl'
	# Private Key for cellar system
	PRIVATE_KEY = 'RHlaNtVzqhpfuih'
	# Group Id
	GROUP_ID = '55648ad2f89fed0300524ff7'
	# Account Id for Bank system
	ACCOUNT_ID = '55648ad3f89fed0300525002'
	# URLs for external systems
	CELLAR_URL = 'http://integracion-2015-prod.herokuapp.com/bodega'
	BANK_URL = 'http://moyas.ing.puc.cl:8080/TDIg3/webresources/bank'
	#BANK_URL = 'http://moyas.ing.puc.cl/apolo'
	BILLING_URL = 'http://moyas.ing.puc.cl:8080/TDIg3/webresources/bill_system'
	#BILLING_URL = 'http://moyas.ing.puc.cl/zeuz'
	#OC_URL = 'http://moyas.ing.puc.cl/atenea'
	OC_URL = 'http://moyas.ing.puc.cl:8080/TDIg3/webresources/oc_system'

	RECEPTION_IDS = {
		'1': '556489daefb3d7030091bab3',
		'2': '556489daefb3d7030091bab5',
		'3': '556489daefb3d7030091bab7',
		'4': '556489daefb3d7030091bab9',
		'5': '556489daefb3d7030091bab2',
		'6': '556489daefb3d7030091bab4',
		'7': '556489daefb3d7030091bab6',
		'8': '556489daefb3d7030091bab8'
	}

else:
	# MULE
	MULE_URL = 'http://chiri.ing.puc.cl/integra3'
	# ODOO
	ODOO_URL = 'chiri.ing.puc.cl'
	# Ftp host
	FTP_HOST = 'chiri.ing.puc.cl'
	# Private Key for cellar system
	PRIVATE_KEY = 'VNfehukMhHGSDr'
	# Group Id
	GROUP_ID = '556489daefb3d7030091baac'
	# Account Id for Bank system
	ACCOUNT_ID = '556489daefb3d7030091bab7'
	# URLs for external systems
	CELLAR_URL = 'http://integracion-2015-dev.herokuapp.com/bodega'
	#BANK_URL = 'http://chiri.ing.puc.cl/apolo'
	BANK_URL = 'http://chiri.ing.puc.cl:8080/TDIg3/webresources/bank'
	#BILLING_URL = 'http://chiri.ing.puc.cl/zeuz'
	BILLING_URL = 'http://chiri.ing.puc.cl:8080/TDIg3/webresources/bill_system'
	#OC_URL = 'http://chiri.ing.puc.cl/atenea'
	OC_URL = 'http://chiri.ing.puc.cl:8080/TDIg3/webresources/oc_system'

	RECEPTION_IDS = {
		'1': '556489e6efb3d7030091bac2',
		'2': '556489e7efb3d7030091bb7d',
		'3': '556489e7efb3d7030091bc67',
		'4': '556489e7efb3d7030091bd07',
		'5': '556489e7efb3d7030091bdcc',
		'6': '556489e7efb3d7030091bf2b',
		'7': '556489e7efb3d7030091bf6f',
		'8': '556489e7efb3d7030091bf89'
	}

# FTP
FTP_PASSWORD = 'M9yC.3$Qz'

# ODOO
ODOO_PORT = 8070
ODOO_DB_NAME = 'INTEGRA3'
ODOO_USERNAME = 'admin'
ODOO_PASSWORD = FTP_PASSWORD

# INSTAGRAM
INSTAGRAM_CLIENT_ID = '5cedcb2318894fe78386e7fd80eafe88'
INSTAGRAM_CLIENT_SECRET = 'f93ac446732344fe85bea1f34ea2e893'
INSTAGRAM_CALLBACK = 'http://integra3.ing.puc.cl/new_promo/'


# TWITTER
TWITTER_ID = '3342072748'
TWITTER_API_KEY = '4mY7HsXARtGwWu9bphP0JgaaV'
TWITTER_API_KEY_SECRET = '27iXp380LCOqEYKznfR4YbrJxR1JIgfww6qN5VVdm8DneipRpA'
TWITTER_ACCESS_TOKEN = '3342072748-XQjWTEnAjVM9KV86ysB4fyPxdpFCCezRtukmvYn'
TWITTER_ACCESS_TOKEN_SECRET = 'HZGZ4h3W2ZWYUbDP4xLSLPkT49d17xGnal2PYghsjUvvT'
TWITTER_URL = 'https://twitter.com/TI_grupo3_2015/status/'

# Cellar info
ADDRESS = 'Rigillis 15, 10674 Atenas'
PHONE = '(30) 210 7292847'
CONTINENT = 'Europa'
COUNTRY = 'Grecia'

# URLs, tokens and addresses for b2b clients by client ID
CLIENTS = {
	'1': {
		'url': 'http://integra1.ing.puc.cl/api',
		'token': 'Z3J1cG8zZ3J1cG8z',
		'address': 'Mohrenstraße 82, 10117 Berlin, Germany',
		'bank_account': '',
		'odoo_id': 11,
		'odoo_provider_id': 0
	},
	'2': {
		'url': 'http://integra2.ing.puc.cl',
		'token': 'token2',
		'address': 'Denezhnyy per., 9 стр. 1, Moscow, Russia',
		'odoo_id': 22,
		'odoo_provider_id': 0
	},
	'3': {
		'url': 'http://integra3.ing.puc.cl/b2b',
		'token': 'bb6004495fa6a785cb5dbf25ba4d81146ad1aa4a',
		'address': 'Denezhnyy per., 9 стр. 1, Moscow, Russia',
		'bank_account': '',
		'odoo_id': 0,
		'odoo_provider_id': 0
	},
	'4': {
		'url': 'http://integra4.ing.puc.cl/b2b',
		'token': '42621462780fbcaeaa5f35116c6b91ef',
		'address': 'Nº2 Saleh Ayoub Suite 41, Zamalek, Cairo',
		'bank_account': '',
		'odoo_id': 29,
		'odoo_provider_id': 8
	},
	'5': {
		'url': 'http://integra5.ing.puc.cl/b2b',
		'token': 'NKHjd1u2JMveF4ECbdWhcA',
		'address': '2290 Yan An West Road. Shanghai',
		'bank_account': '',
		'odoo_id': 24,
		'odoo_provider_id': 10
	},
	'6': {
		'url': 'http://integra6.ing.puc.cl',
		'token': '4925e9fa00d3ed9a828bec5114ba9387',
		'address': 'Calle de Lagasca, 89, 29010 Madrid, Spain',
		'bank_account': '',
		'odoo_id': 25,
		'odoo_provider_id': 6
	},
	'7': {
		'url': 'http://integra7.ing.puc.cl/api',
		'token': 'f0bc8cc4561352aa0bc0d4933ef38526',
		'address': "10 Culgoa CircuitO'Malley, australia",
		'bank_account': '',
		'odoo_id': 26,
		'odoo_provider_id': 7
	},
	'8': {
		'url': 'http://integra8.ing.puc.cl/b2b',
		'token': '4159740b26a92664b862fa0120912458',
		'address': "169 Garsfontein Road,  Delmondo Office Park, Sudafrica",
		'bank_account': '',
		'odoo_id': 27,
		'odoo_provider_id': 9
	},
	'9': {
		'url': 'http://www.vital.com.ar',
		'token': '',
		'address': "Balvanera, Buenos Aires, Argentina",
		'bank_account': '',
		'odoo_id': 28,
		'odoo_provider_id': 0
	},
	'10': {
		'url': 'http://www.atomoconviene.com',
		'token': '',
		'address': "Alvarez Condarco 740, Las Heras, Mendoza, Argentina",
		'bank_account': '',
		'odoo_id': 12,
		'odoo_provider_id': 0
	},
	'11': {
		'url': 'https://plus.google.com/100984494934590358450/about?hl=en',
		'token': '',
		'address': "Avda Cristobal Colon 3707, Santiago",
		'bank_account': '',
		'odoo_id': 13,
		'odoo_provider_id': 0
	},
	'12': {
		'url': 'http://www.trome.pe',
		'token': '',
		'address': "Republica de Chile 504, Jesús María, Peru",
		'bank_account': '',
		'odoo_id': 14,
		'odoo_provider_id': 0
	},
	'13': {
		'url': 'http://www.sluckis.com.uy',
		'token': '',
		'address': "Arenal Grande 2193, Montevideo, Uruguay",
		'bank_account': '',
		'odoo_id': 15,
		'odoo_provider_id': 0
	},
	'14': {
		'url': 'http://http://www.dmc.bo/',
		'token': '',
		'address': "Anahí, Santa Cruz de la Sierra, Bolivia",
		'bank_account': '',
		'odoo_id': 16,
		'odoo_provider_id': 0
	},
	'15': {
		'url': 'http://www.fastrax.com.py/',
		'token': '',
		'address': "Tte. Fariña Nº 166 esq. Yegros",
		'bank_account': '',
		'odoo_id': 17,
		'odoo_provider_id': 0
	},
	'16': {
		'url': 'http://www.mps.com.co',
		'token': '',
		'address': "45 CC Monterrey Locales 326 y 327, Carrera 50 # 10, Medillin, Antioquia, Colombia",
		'bank_account': '',
		'odoo_id': 18,
		'odoo_provider_id': 0
	},
	'17': {
		'url': 'http://www.kode-tech.com',
		'token': '',
		'address': "Av. Sanatorio del Ávila, Edif. Yacambú, Piso 3, Boleita Norte, Caracas, Venezuela.",
		'bank_account': '',
		'odoo_id': 19,
		'odoo_provider_id': 0
	},
	'18': {
		'url': 'http://www.mambo.com.br',
		'token': '',
		'address': "Rua Deputado Lacerda Franco, 553 - Pinheiros, Sao Paulo - SP, Brazil",
		'bank_account': '',
		'odoo_id': 20,
		'odoo_provider_id': 0
	},
	'19': {
		'url': 'http://www.dcm.com.mx',
		'token': '',
		'address': "Laguna de Mayran 300, Anahuac, Miguel Hidalgo, Ciudad de Mexico, D.F., Mexico.",
		'bank_account': '',
		'odoo_id': 21,
		'odoo_provider_id': 0
	},
	'20': {
		'url': 'http://www.greenfieldusaauto.com',
		'token': '',
		'address': "5641 Dewey St, Hollywood, FL, United States",
		'bank_account': '',
		'odoo_id': 23,
		'odoo_provider_id': 0
	},
	'21': {
		'url': 'http://www.toptenwholesale.com',
		'token': '',
		'address': "448 S Hill St #712, Los Angeles, CA, United States",
		'bank_account': '',
		'odoo_id': 30,
		'odoo_provider_id': 0
	}
}



