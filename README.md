# PURCHASE SYSTEM

Sistema que se encarga de procesar las ordenes de compra provenientes de cualquier canal de ventas.



## Prerequisites

- [Redis](http://redis.io/)
- [MongoDB](https://www.mongodb.org/)

## Install

```sh
git clone git@bitbucket.org:tdi2015grupo3/purshase-system.git purchase-system
cd purchase-system
virtualenv venv
source venv/bin/activate
pip install -r requierements.txt
```
