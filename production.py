# Productos

#sku: 14
cebada = {
	'name': 'Cebada',
	'odoo_id': 8,
	'production_cost': 3673,
	'type': 'Raw Material',
	'category': 'Cereales y Legumbres',
	'unit': 'UN',
	'production_batch': 1,
	'production_time': 1.2
}
#sku: 15
avena = {
	'name': 'Avena',
	'odoo_id': 9,
	'production_cost': 3660,
	'type': 'Raw Material',
	'category': 'Cereales y Legumbres',
	'unit': 'UN',
	'production_batch': 1,
	'production_time': 1.9
}
#sku: 16
pasta_trigo = {
	'name': 'Pasta de Trigo',
	'odoo_id': 10,
	'production_cost': 1251,
	'type': 'Finished Goods',
	'category': 'Cereales y Legumbres',
	'unit': 'UN',
	'production_batch': 1000,
	'production_time': 2.3,
	'components': [
		{
			'sku': '23',
			'requirement': 330,
			'proveedor': '4',
			'production_cost': 2747,
			'production_batch': 300,
			'production_time': 2.6
		},
		{
			'sku': '26',
			'requirement': 313.3333,
			'proveedor': '5',
			'production_cost': 1946,
			'production_batch': 1,
			'production_time': 1.1
		},
		{
			'sku': '2',
			'requirement': 383.3333,
			'proveedor': '7',
			'production_cost': 1289,
			'production_batch': 1,
			'production_time': 2
		}
	]
}
#sku: 17
cereal_arroz = {
	'name': 'Cereal de Arroz',
	'odoo_id': 11,
	'production_cost': 2602,
	'type': 'Finished Goods',
	'category': 'Cereales y Legumbres',
	'unit': 'UN',
	'production_batch': 1000,
	'production_time': 2.7,
	'components': [
		{
			'sku': '25',
			'requirement': 360,
			'proveedor': '8',
			'production_cost': 1588,
			'production_batch': 400,
			'production_time': 1.7
		},
		{
			'sku': '20',
			'requirement': 350,
			'proveedor': '3',
			'production_cost': 3953,
			'production_batch': 200,
			'production_time': 2.1
		},
		{
			'sku': '13',
			'requirement': 290,
			'proveedor': '4',
			'production_cost': 2780,
			'production_batch': 400,
			'production_time': 1.7
		}
	]
}
#sku: 18
pastel = {
	'name': 'Pastel',
	'odoo_id': 12,
	'production_cost': 3518,
	'type': 'Finished Goods',
	'category': 'Cereales y Legumbres',
	'unit': 'UN',
	'production_batch': 200,
	'production_time': 3.6,
	'components': [
		{
			'sku': '23',
			'requirement': 72,
			'proveedor': '4',
			'production_cost': 2747,
			'production_batch': 300,
			'production_time': 2.6
		},
		{
			'sku': '2',
			'requirement': 71.3333,
			'proveedor': '7',
			'production_cost': 1289,
			'production_batch': 1,
			'production_time': 2
		},
		{
			'sku': '7',
			'requirement': 66.6666,
			'proveedor': '6',
			'production_cost': 1696,
			'production_batch': 1,
			'production_time': 2
		}
	]
}
#sku: 20
cacao = {
	'name': 'Cacao',
	'odoo_id': 13,
	'production_cost': 3953,
	'type': 'Raw Material',
	'category': 'Dulce',
	'unit': 'UN',
	'production_batch': 200,
	'production_time': 2.1
}

harina = {
	'name': 'Harina',
	'odoo_id': 5,
	'sku': '23',
	'requirement': 330,
	'proveedor': '4',
	'production_cost': 2747,
	'production_batch': 300,
	'production_time': 2.6
}

sal = {
	'name': 'Sal',
	'odoo_id': 7,
	'sku': '26',
	'requirement': 313.3333,
	'proveedor': '5',
	'production_cost': 1946,
	'production_batch': 1,
	'production_time': 1.1
}

huevo = {
	'name': 'Huevo',
	'odoo_id': 3,
	'sku': '2',
	'requirement': 383.3333,
	'proveedor': '7',
	'production_cost': 1289,
	'production_batch': 1,
	'production_time': 2
}

azucar = {
	'name': 'Azucar',
	'odoo_id': 6,
	'sku': '25',
	'requirement': 360,
	'proveedor': '8',
	'production_cost': 1588,
	'production_batch': 400,
	'production_time': 1.7
}
		
arroz = {
	'name': 'Arroz',
	'odoo_id': 4,
	'sku': '13',
	'requirement': 290,
	'proveedor': '4',
	'production_cost': 2780,
	'production_batch': 400,
	'production_time': 1.7
}

leche = {
	'name': 'Leche',
	'odoo_id': 2,
	'sku': '7',
	'requirement': 66.6666,
	'proveedor': '6',
	'production_cost': 1696,
	'production_batch': 1,
	'production_time': 2
}

PRODUCTS = {
	'14': cebada,
	'15': avena,
	'16': pasta_trigo,
	'17': cereal_arroz,
	'18': pastel,
	'20': cacao,
	'23': harina,
	'26': sal,
	'2': huevo,
	'25': azucar,
	'13': arroz,
	'7': leche
}